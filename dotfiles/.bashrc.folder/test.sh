c () {
  if [ $# -eq 0 ]; then cd $HOME; return; fi
  cd "$1"
  if [ -d .git ]; then ggs; fi
}
