case $HOSTNAME in
  *xub) hostname_color="00;33";;
  *devstack) hostname_color="00;35";;
  *monasca) hostname_color="00;34";;
esac

find_git_dirty() {
  local status=$(git status --porcelain 2> /dev/null)
  if [[ "$status" != "" ]]; then
    return '*'
  else
    return '#'
  fi
}


# PS1
myPs1=""
myPs1=$myPs1"\[\e[01;10m\]---\[\e[0m\] "
myPs1=$myPs1"\[\e["$hostname_color"m\]\u\[\e[0m\] "
myPs1=$myPs1"\[\e[01;10m\]@\[\e[0m\] "
myPs1=$myPs1"\[\e["$hostname_color"m\]\h\[\e[0m\] "
myPs1=$myPs1"\[\e[00;36m\]\w\[\e[0m\]"
myPs1=$myPs1"\[\e[01;30m\]\$(__git_ps1)\[\e[0m\] "
myPs1=$myPs1"\[\e[01;30m\][\$?]\[\e[0m\]\n  "
myPs1=$myPs1"\[\e[01;34m\]\\$\[\e[0m\] "
export PS1=$myPs1
