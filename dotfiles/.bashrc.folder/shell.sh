freeupmem() {
  sudo -s -- bash -c 'free && sync && echo 3 > /proc/sys/vm/drop_caches && free'
}

c_old() {
  if [ $# -eq 0 ]; then cd $HOME; return; fi
  if [ "$1" == "d" ]; then
     if [ -z "$2" ]; then cd ~/dev; return; fi
     if [ "$2" == "p" ]; then cd ~/dev/python; return; fi
     if [ "$2" == "w" ]; then cd ~/dev/web; return; fi
  fi
  if [ "$1" == "v" ]; then
     if [ -z "$2" ]; then cd ~/vagr; return; fi
     if [ "$2" == "v" ]; then cd ~/vagr/monasca-vagrant; return; fi
  fi
  if [ "$1" == "s" ]; then
     if [ -z "$2" ]; then cd ~/src; return; fi
     if [ "$2" == "d" ]; then cd ~/src/dotfiles; return; fi
  fi
}

=() {
    calc="${@//p/+}"
    calc="${calc//x/*}"
    bc -l <<<"scale=10;$calc"
}

search() {
  tosearch=""
  if [ ! $# -gt 1 ]; then echo "No arguments supplied"; return 1; fi
  c=0
  for var in "$@"; do
    [ $c -eq 0 ] && cmd=$var || tosearch="$tosearch $var"
    (( c++ ))
  done
  tosearch=${tosearch:1}
  echo "searching... \"$tosearch\""
  FoundFileCount=0
  exitval="EXIT"
  ( grep "$tosearch" -R --color=always . && echo "$exitval") | while read line
  do
    if [ "$line" == "$exitval" ]; then
      echo "-----CHOOSE FILE"
      read input </dev/tty
      re='^[0-9]+$'
      if ! [[ $input =~ $re ]] ; then echo "quitting..." >&2; return 2; fi
      echo "${FoundFile[$input]}" | sed -r 's/\x1B\[([0-9]{1,3}((;[0-9]{1,3})*)?)?[m|K]//g' | while read line
      do
        #line=${line%?}
        if [ "$cmd" == "x" ]; then
          echo -n "$line" | xclip -selection c
        else
          temp="$cmd $PWD/$line"
          echo $temp
          eval $temp
        fi
        return 0
      done
    else
      count=0
      IFS=':' read -ra OUTLINE <<< "$line"
      for i in "${OUTLINE[@]}"; do
        if [ $count -eq 0 ]; then
          FoundFile[$FoundFileCount]="$i"
          echo -n $FoundFileCount" => "
        fi
        echo -n " $i"
        (( count++ ))
      done
      echo " "
      (( FoundFileCount++ ))
    fi
  done
}

alias searchc="search ccat "
alias csearch="search ccat "
alias searchx="search x "
alias xsearch="search x "
alias searchv="search vim "
alias vsearch="search vim "

# source ~/src/autoenv/activate.sh/
