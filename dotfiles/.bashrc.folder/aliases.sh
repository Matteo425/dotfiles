# ALIASES
alias ls='ls --color'
alias ll='ls -AlhF --time-style="+%d/%m/%y" --group-directories-first'
alias la='ls -A'
alias l='ls -CF'
alias l="ls -lhF --time-style='+%d/%m/%y' --group-directories-first"
lso() { ls -alG "$@" | awk '{k=0; for(i=0;i<=8;i++) k+=((substr($1,i+2,1)~/[rwx]/)*2^(8-i)); if(k) printf(" %0o ",k);print}'; }

alias foldersize="du -sh *"

alias v="vim"
alias g="git"

alias gl="g l"
alias ggl="g l"
alias ggc="g checkout"
alias ggm="g merge"
alias ggf="g fetch"
alias ggp="g push"
alias ggs="g status -sb"
alias gga="g add"
alias ggc="g commit"
alias ggc="g commit"

alias tar-ext="tar zxvf"

alias setclip='xclip -selection c'
alias getclip='xclip -selection clipboard -o'

alias ccat='pygmentize -g'

alias inst='sudo apt-get install --no-install-recommends'

alias cpuuse='cut -d " " -f 1-3 /proc/loadavg'
alias reloadBash='source ~/.bashrc'

alias exemon='ssh vagrant@monasca monasca --json --os_username=mini-mon --os_password=password --os_auth_url http://devstack:5000/v3/'

alias mtmux='tmux attach-session -t base || tmux new -s base'

alias pass="/home/mat/src/kpcli.pl --kdb='/home/mat/Dropbox/Apps/KeePass/mydb.kdbx'"

alias jj="getclip | sed \"s/'{/{/\" | sed \"s/}'/}/\" | python -m json.tool > /tmp/temp.json && vim /tmp/temp.json"

alias ..="cd .."
alias cd..="cd .."

alias diff="colordiff"

alias mount="mount | column -t"
alias ports="netstat -tulanp"

alias tf="tail -f"

alias gitinfo="git fetch && echo \"---\" && git status -sb && echo \"---\" && git l"

alias snip="/home/mat/src/sssnippets/env/bin/python /home/mat/src/sssnippets/snip.py"


