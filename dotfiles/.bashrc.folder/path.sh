# JAVA VARS
# export JAVA_HOME="/usr/lib/jvm/default-java/"
# export JRE_HOME="/usr/lib/jvm/default-java/jre/bin/java"
# export PATH=$PATH:$HOME/bin:${JAVA_HOME}:${JRE_HOME}
# PATH MODS
# export PATH=$PATH:/opt/android-studio/bin
# export PATH=$PATH:/opt/eclipse
# export PATH=$PATH:/opt/Telegram
# export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

modload() {
  tosearch=""
  if [ $# -eq 0 ]; then
      echo "No arguments supplied"
      echo "Available mods are: node, go, redis, ruby, fuck"
      return 1
  fi

  if [ "$1" == "node" ]; then
    if [ ! -d "$HOME/.nvm" ]; then
      echo "Nvm folder in $HOME/.nvm not found"
      return 2
    fi
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
    nvm use stable

  elif [ "$1" == "go" ]; then
    if [ ! -d "/usr/local/go" ]; then
      echo "Go folder in /usr/local/go not found"
      return 2
    fi
    export PATH=$PATH:/usr/local/go/bin
    export GOPATH="$HOME/work"
    export PATH=$PATH:$GOPATH/bin

  elif [ "$1" == "ruby" ]; then
    if [ ! -d "$HOME/.rvm" ]; then
      echo "Rvm folder in $HOME/.rvm not found"
      return 2
    fi
    export PATH="$PATH:$HOME/.rvm/bin" 
    source ~/.rvm/scripts/rvm

  elif [ "$1" == "redis" ]; then
    if [ ! -d "$HOME/src/redis" ]; then
      echo "Redis folder in $HOME/src/redis not found"
      return 2
    fi
    export PATH=$PATH:$HOME/src/redis/src

  elif [ "$1" == "fuck" ]; then
    eval $(thefuck --alias)

  else
    echo "Wrong command"
    echo "Available mods are: node, go, redis, ruby, fuck"
    return 1
  fi
}



