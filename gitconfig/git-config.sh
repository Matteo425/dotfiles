git config --global alias.hist "log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short"
git config --global alias.l "log --graph --decorate --pretty=oneline --abbrev-commit --all"
git config --global alias.lol "log --graph --decorate --pretty=oneline --abbrev-commit --all"
git config --global alias.mylog "log --pretty=format:'%h %s [%an]' --graph"
git config --global user.name "Matteo Caruso"
git config --global user.email "m.caruso425@gmail.com"
git config --global push.default matching

