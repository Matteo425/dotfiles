#!/bin/bash

###
# DATA
###

# init global vars
SOURCE="${BASH_SOURCE[0]}"
SCRIPTPATH=$(pwd)/$(dirname $SOURCE)/
dotFold=$SCRIPTPATH/
homeFold=~/
confFold=$homeFold".config/"
bashrcFile=$homeFold".bashrc"
# load modules
for f in bash_mod/*; do
  . $f
done
# load config
. $dotFold'config.ini'
if [ -z "$dropFold" ]; then dropFold=$dotFold; fi

# init data
datadot=(\
 $dotFold"dotfiles/" $homeFold ".bashrc.folder"\
 $dotFold"dotfiles/" $homeFold ".vimrc"\
 $dotFold"dotfiles/" $homeFold ".tmux.conf"\
 $dotFold"dotfiles/" $homeFold ".toprc"\
 $dotFold"dotfiles/" $homeFold ".gitconfig"\
)
datasubl=(\
 $dropFold"sublime/sublime-text-3/" $confFold"sublime-text-3/" "Installed Packages"\
 $dropFold"sublime/sublime-text-3/" $confFold"sublime-text-3/" "Packages"\
)
datavim=(\
 $dropFold"vim/" $homeFold ".vim"\
)
datatmux=(\
 $dropFold"tmux/" $homeFold ".tmux"\
)
datassh=(\
 $dropFold"ssh/" $homeFold ".ssh"\
)
datazilla=(\
 $dropFold"filezilla/" $homeFold ".filezilla"\
)
dataxfce=(\
 $dotFold"xfce/" $confFold"xfce4/" "xfconf"\
)
datadep=(\
  "apt" "git"\
  "apt" "unzip"\
  "apt" "xclip"\
  "pip" "Pygments"\
)


###
# EXECUTE
###
OK_ARGS=false
for i in "$@"; do
  case $i in
    -help)
      echo 'Use one (or more) of this options:'
      echo '-----------------------------------------------------------------'
      echo '-rm:        force the linking'
      echo ' '
      echo '-dot:       link the standard dot files'
      echo ' '
      echo '-tmux-inst: install tmux'
      echo '-tmux-dl:   download (link on config.ini) tmux folder and link it'
      echo '-tmux-fold  link the tmux folder from dropFold (config.ini)'
      echo ' '
      echo '-vim-dl:    (link on config.ini) vim folder and link it'
      echo '-vim-fold:  link the vim folder from dropFold (config.ini)'
      echo ' '
      echo '-subl-dl:   download (link on config.ini) subl folder and link it'
      echo '-subl-fold: link the subl folder from dropFold (config.ini)'
      echo ' '
      echo '-ssh:       link the ssh folder from dropFold (config.ini)'
      echo ' '
      echo '-git:       set up global git params'
      echo ' '
      echo '-xfce:      link the xfce folder'
      echo ' '
      echo '-zilla:     link the filezilla folder from dropFold (config.ini)'
      echo ' '
      echo '-help:      print this sheet and exits'
      echo '-----------------------------------------------------------------'
      exit 0
    ;;

    -dep)
      printprog 0 "check dependencies"
      len=${#datadep[@]}
      len=$((len - 1))
      for i in $(seq 0 2 $len); do
        if [ "${datadep[$i]}" == "apt" ]; then
          checkDepApt "${datadep[$((i+1))]}"
        elif [ "${datadep[$i]}" == "pip" ]; then
          checkDepPip "${datadep[$((i+1))]}"
        fi
      done
      OK_ARGS=true
    ;;

    -rm)
      dFRM=true
      OK_ARGS=true
    ;;

    -dot)
      printprog 0 "dot files"
      makeLinks datadot
      if grep -q "CUSTOMIMPORT" $bashrcFile; then
        printavo 1 "bashrc file already setted"
      else
        cat $dotFold"dotfiles/.bashrc.append" >> $bashrcFile
        printok 1 "bashrc modified correctly"
      fi
      source $bashrcFile
      OK_ARGS=true
    ;;

    -tmux-inst)
      printprog 0 "install tmux"
      install_script libevent https://sourceforge.net/projects/levent/files/libevent/libevent-2.0/libevent-2.0.22-stable.tar.gz
      install_script tmux2 https://github.com/tmux/tmux/releases/download/2.0/tmux-2.0.tar.gz
      OK_ARGS=true
    ;;

    -tmux-dl)
      printprog 0 "download tmux folder"
      downloadAndLink tmux
      OK_ARGS=true
    ;;

    -tmux-fold)
      printprog 0 "link tmux from folder"
      makeLinks datatmux
      OK_ARGS=true
    ;;

    -vim-dl)
      printprog 0 "download vim folder"
      downloadAndLink vim
      OK_ARGS=true
    ;;

    -vim-fold)
      printprog 0 "link vim folder"
      makeLinks datavim
      OK_ARGS=true
    ;;

    -subl-dl)
      printprog 0 "download subl folder"
      downloadAndLink subl
      OK_ARGS=true
    ;;

    -subl-fold)
      printprog 0 "link subl folder"
      makeLinks datasubl
      OK_ARGS=true
    ;;

    -ssh)
      printprog 0 "link ssh from folder"
      makeLinks datassh
      printok 1 "fix ssh perms"
      sshF=~/.ssh/id_rsa
      chmod 0600 $sshF
      OK_ARGS=true
    ;;

    -zilla)
      printprog 0 "link filezilla from folder"
      makeLinks datazilla
      OK_ARGS=true
    ;;

    -xfce)
      printprog 0 "link xfce config"
      makeLinks dataxfce
      OK_ARGS=true
    ;;
  esac
done
if ! $OK_ARGS; then
  echo "NO VALID ARGS:"
  echo "--> \"${dotFold}setup.sh -help\" for command list"
fi
echo ''

