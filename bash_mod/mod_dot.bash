makeLink() {
  dFRM=false
  srcFold=$1
  dstFold=$2
  srcFile=$1$3
  dstFile=$2$3
  if [ -r "$dstFile" ]; then
    if $dFRM; then
      rm -i "$dstFile"
      if [ $? -eq 0 ]; then
        ln -s "$srcFile" "$dstFold" &&
        printok 1 "file removed and link $3 made"
      else printerr 1 "not removed"
      fi
    else printavo 1 "link of $3 already exists"
    fi
  else
    ln -s "$srcFile" "$dstFold"
    printok 1 "link $3 made"
  fi
}

makeLinks() {
  name=$1[@]
  d=("${!name}")
  len=${#d[@]}
  len=$((len - 1))
  for i in $(seq 0 3 $len)
  do
    makeLink "${d[$i]}" "${d[$((i+1))]}" "${d[$((i+2))]}"
  done
}

downloadAndLink() {
  obj=$1
  uuu=$1"UrlZip"
  cd $dropFold &&
  wget -q --progress=bar:force --content-disposition ${!uuu} -O temp.zip &&
  unzip -q temp.zip -d $obj &&
  rm $obj".zip" &&
  printprog 1 "make $obj links" &&
  makeLinks data$obj
}
