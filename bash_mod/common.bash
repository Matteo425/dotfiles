printout() {
  for i in `seq 0 1 $1`; do
    echo -n "   "
  done
  echo "($2) $3"
}
printok() { printout $1 'v' "OK: $2"; }
printprog() { printout $1 ' ' "PROGRESS: ${2}"; }
printerr() { printout $1 'x' "ERROR: $2"; }
printavo() { printout $1 '-' "AVOID: $2"; }

