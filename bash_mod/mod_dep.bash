checkDepApt() {
  dpkg -l | grep $1 > /dev/null
  if [ $? -eq 1 ]; then
    printprog 1 "apt dep $1 not satisfied, install"
    sudo apt-get install -y -q $1
  else
    printok 1 "apt dep $1 satisfied"
  fi
}

checkDepPip() {
  pip freeze | grep "$1" > /dev/null
  if [ $? -eq 1 ]; then
    printprog 1 "pip dep $1 not satisfied, install"
    sudo pip --quiet install $1
  else
    printok 1 "pip dep $1 satisfied"
  fi
}

