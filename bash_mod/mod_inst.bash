install_script() {
  obj=$1
  url=$2
  cd $dotFold
  printprog 2 "download $obj" &&
  wget -q $url -O $obj".tar.gz" &&
  printprog 2 "extract $obj" &&
  mkdir $dotFold$obj &&
  tar zxf $obj.tar.gz -C $obj --strip-components 1 &&
  rm $obj.tar.gz &&
  cd $dotFold$obj &&
  printprog 2 "compiling $obj" &&
  ( ./configure > ../configure.out ) &&
  rm ../configure.out
  ( make --quiet -s --silent > ../make.out ) &&
  rm ../make.out &&
  printprog 2 "installing $obj" &&
  sudo make --quiet install &&
  printok 2 "installed"
}

